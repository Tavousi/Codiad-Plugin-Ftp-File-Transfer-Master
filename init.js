/*
* Created by @Tavousi | http://tavousi.name
* all rights reserverd for @Tavousi | p30search@gmail.com
* Tell : +98 9381585940
*/
(function(global, $){
		
    var codiad = global.codiad,
        scripts= document.getElementsByTagName('script'),
        path = scripts[scripts.length-1].src.split('?')[0],
        curpath = path.split('/').slice(0, -1).join('/')+'/';
		
    $(function() {    
        codiad.ftp.init();
    });

    codiad.ftp = {
		
        dialog: curpath + 'dialog.php',
		
        path: curpath,
		
        init: function() {
		},
		
        open: function() {
            var _this = this;
            codiad.modal.load(800, _this.dialog);
        },
		
		FTP: function() {
            var ftp = document.getElementById("ftp_adress").value;
            var port = document.getElementById("ftp_port").value;
            var user = document.getElementById("ftp_user").value;
            var pass = document.getElementById("ftp_pass").value;
			var folder = document.getElementById("ftp_folder").value;
            var path = document.getElementById("ftp_path").value;
            var type = document.getElementById("ftp_type").value;
            var save = document.getElementById("ftp_save").checked;
            $('#ecld').fadeIn('slow');
			$('#buttonhid').attr('style', 'display:none;');
            $.post(this.path+'ftp/ftp.connection.php',
	        'ftp='+ftp
           	+'&user='+user
        	+'&pass='+pass
        	,function(data){
	        	if(data == 'ok') {
			      $('#alertt').slideUp('fast');
                  $('#details').html('<font color="green">Successfully connected to the server ||| Uploading ...</font><hr style="margin-bottom:15px;" />');
                  $('#alertt').slideDown('fast');
                  return codiad.ftp.FTPUPLOAD(ftp,user,pass,folder,path,type);
		        } else {
                  $('#alertt').slideUp('fast');
                  $('#details').html(data);
                  $('#alertt').slideDown('fast');
				  $('#buttonhid').attr('style', 'display:inline;');
                  $('#ecld').fadeOut('slow');
		        }
           }); 
        },	
		
		FTPUPLOAD: function(ftp,user,pass,folder,path,type) {
				  $.post(this.path+'ftp/ftp.upload.php',
	              'ftp='+ftp
                  +'&user='+user
        	      +'&pass='+pass
                  +'&folder='+folder
        	      +'&path='+path
                  +'&type='+type
        	      ,function(data){
					  $('#alertt').slideUp('fast');
					  $('#details').html(data);
					  $('#alertt').slideDown('fast');
					  $('#buttonhid').attr('style', 'display:inline;');
					  $('#ecld').fadeOut('slow');
		          }); 	
		},
				
		clearForm: function() {
            $('#ftp_adress').attr('value', '');
            $('#ftp_user').attr('value', '');
            $('#ftp_pass').attr('value', '');
        },	
			
    };
	
})(this, jQuery);
